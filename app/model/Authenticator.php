<?php

namespace App\Model;

class Authenticator extends \Nette\Object implements \Nette\Security\IAuthenticator {

    /** @var \Dibi\Connection */
    protected $db;

    public function __construct(\Dibi\Connection $db) {
        $this->db = $db;
    }

    public function authenticate(array $credentials) {
        list($username, $password) = $credentials;

        $row = $this->db->select('*')
            ->from('user')
            ->where('username = %s', $username)
            ->fetch();

        if(!$row) {
            throw new \Nette\Security\AuthenticationException('User not found.');
        }

        if(!\Nette\Security\Passwords::verify($password, $row->password)) {
            throw new \Nette\Security\AuthenticationException('Invalid password!');
        }

        return new \Nette\Security\Identity($row->id, [], $row->toArray());
    }

}
