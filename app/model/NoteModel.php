<?php

namespace App\Model;


class NoteModel implements INoteModel {

    const DATA_FILE = __DIR__ . '/../data/notes.json';

    public function get($userId = null) {
        $data = $this->getData();

        if($userId && $data) {
            foreach ($data as $key => $item) {
                if($item['user_id'] != $userId) unset($data[$key]);
                else {
                    $data[$key] = (object)$item;
                }
            }
        }

        return (array)($data ?: []);
    }

    public function add($values) {
        $data = $this->getData();

        $data[] = [
            'title' => $values['title'],
            'text' => $values['text'],
            'user_id' => $values['user_id'],
            'created' => (new \DateTime())->format('Y-m-d H:i:s'),
        ];

        file_put_contents(self::DATA_FILE, json_encode($data));
    }

    public function edit($id, $values) {
        $data = $this->getData();

        $data[$id] = [
            'title' => $values['title'],
            'text' => $values['text'],
            'created' => $data[$id]['created'],
            'user_id' => $data[$id]['user_id'],
        ];

        file_put_contents(self::DATA_FILE, json_encode($data));
    }

    public function delete($id) {
        $data = $this->getData();

        unset($data[$id]);

        file_put_contents(self::DATA_FILE, json_encode($data));
    }

    protected function getData() {
        $data = json_decode(file_get_contents(self::DATA_FILE), true);

        return $data;
    }

}