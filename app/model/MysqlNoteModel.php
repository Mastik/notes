<?php

namespace App\Model;


class MysqlNoteModel implements INoteModel {

    /** @var \Dibi\Connection */
    protected $db;

    const TABLE_NAME = 'note';

    /**
     * MysqlNoteModel constructor.
     * @param \Dibi\Connection $db
     */
    public function __construct(\Dibi\Connection $db) {
        $this->db = $db;
    }

    public function get($userId = null) {
        $data = $this->db->select('*')->from(self::TABLE_NAME)->orderBy('created');

        if($userId) {
            $data->where('user_id = %i', $userId);
        }

        return $data->fetchAssoc('id');
    }

    public function add($values) {
        $this->db->insert(self::TABLE_NAME, $values)
            ->execute();
    }

    public function edit($id, $values) {
        $this->db->update(self::TABLE_NAME, $values)
            ->where('id = %i', $id)
            ->execute();
    }

    public function delete($id) {
        $this->db->delete(self::TABLE_NAME)
            ->where('id = %i', $id)
            ->execute();
    }

}