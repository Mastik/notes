<?php

namespace App\Model;


interface INoteModel {

    public function get($userId = null);

    public function add($values);

    public function edit($id, $values);

    public function delete($id);

}