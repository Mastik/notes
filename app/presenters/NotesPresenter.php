<?php

namespace App\Presenters;


class NotesPresenter extends \Nette\Application\UI\Presenter {

    /** @var \App\Model\INoteModel @inject */
    public $noteModel;

    public function renderDefault() {
        $this->template->notes = $this->noteModel->get($this->getUser()->getId());
    }

    public function renderEdit($id) {
        $data = $this->noteModel->get($this->getUser()->getId())[$id];

        $this['edit']->setDefaults([
                'title' => $data->title,
                'text' => $data->text,
                'id' => $id,
            ]
        );
    }

    public function actionDelete($id) {
        $this->noteModel->delete($id);

        $this->flashMessage('Poznámka byla smazána', 'info');
        $this->redirect('Notes:');
    }

    public function createComponentAdd($name) {
        $form = new \Nette\Application\UI\Form($this, $name);

        $form->addText('title', 'Titulek');
        $form->addTextArea('text', 'Text poznámky');
        $form->addSubmit('submit', 'Přidat');

        $form->onSuccess[] = [$this, 'successAdd'];

        return $form;
    }

    /**
     * @param \Nette\Application\UI\Form $form
     */
    public function successAdd($form) {
        $values = (array)$form->getValues();

        $values['user_id'] = $this->getUser()->getId();

        $this->noteModel->add($values);

        $this->flashMessage('Přidáno', 'success');
        $this->redirect('default');
    }

    public function createComponentEdit($name) {
        $form = new \Nette\Application\UI\Form($this, $name);

        $form->addText('title', 'Titulek');
        $form->addTextArea('text', 'Text poznámky');
        $form->addHidden('id');
        $form->addSubmit('submit', 'Uložit');

        $form->onSuccess[] = [$this, 'successEdit'];

        return $form;
    }

    /**
     * @param \Nette\Application\UI\Form $form
     */
    public function successEdit($form) {
        $values = (array)$form->getValues();

        $this->noteModel->edit($values['id'], $values);

        $this->flashMessage('Upraveno', 'success');
        $this->redirect('default');
    }

}