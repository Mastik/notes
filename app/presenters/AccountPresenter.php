<?php

namespace App\Presenters;


class AccountPresenter extends \Nette\Application\UI\Presenter {

    public function actionLogout() {
        if($this->getUser()->isLoggedIn()) {
            $this->getUser()->logout(true);
        }

        $this->flashMessage('Byl jste odhlášen', 'info');
        $this->redirect('Homepage:');
    }

    public function renderLogin() {
        if($this->getUser()->isLoggedIn()) $this->redirect('Homepage:');
    }

    public function createComponentLogin($name) {
        $form = new \Nette\Application\UI\Form($this, $name);

        $form->addText('username', 'Jméno');
        $form->addPassword('password', 'Heslo');
        $form->addSubmit('submit', 'Přihlásit');

        $form->onSuccess[] = [$this, 'succeedLogin'];

        return $form;
    }

    public function succeedLogin(\Nette\Application\UI\Form $form) {
        $values = $form->getValues(true);

        try {
            $this->user->login($values['username'], $values['password']);
        }
        catch (\Nette\Security\AuthenticationException $ex) {
            $this->flashMessage($ex->getMessage(), 'danger');
            $this->redirect('loginScreen');
        }

        $this->flashMessage('Byl jste úspěšně přihlášen', 'success');
        $this->redirect('Homepage:');
    }

}